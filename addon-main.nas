var main = func(root)
{

 var modules = ['debriefing','runway_view', 'fwhax'];
 foreach (module; modules) {
  io.load_nasal(root.basePath ~ "/Nasal/" ~ module ~ ".nas", module);
 }

}
