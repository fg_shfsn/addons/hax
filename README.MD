Hax!
====

[https://gitlab.com/femboywings-fgfs/addons/hax](https://gitlab.com/femboywings-fgfs/addons/hax)

Small workarounds and aircraft development tools for FlightGear

Version 1.1

(c) `@femboywings` ([on gitlab.com](https://gitlab.com/femboywings))


Add-on description
------------------

This FlightGear add-on provides the following features.

- Small debriefing dialog with airborne time, landing precision and
  vertical speed.
- Printing of position, fuel and payload to fgfs.log
    - every 5 minutes, for easier restart after FlightGear's crashes;
    - on demand, for manual saving of parked position.
- Dumping of entire property tree to a XML file.
- Disabling TerraSync above a set altitude, to save bandwidth.
- Easy access to JSBSim's terrain and atmosphere overrides.
- Locking of potentially destructive menu items:
    - having no confirmation: "Reset",
      "Aircraft Center (Experimental)", "Random Attitude"
    - being crash-prone "Map (Canvas)".
- Removal of potentially destructive hotkeys: Esc (Exit),
  Shift-Esc (Restart) and Control-U (Add 1000ft).
- Additional views for better judgement of take-off/landing
  recordings:
    - on the side of the touchdown zone;
    - in the end of the runway.
- Additional GUI styles:
    - smaller version of Tortola style, useful for old laptops;
    - bigger version of Tortola style, useful for HiDPI screens;
    - Fly!: style echoing the launcher's look.
- Toggling of FGCOM when Multiplayer goes online/offline.
- Controlling recording resolution and duration.
- Replacement of the blue-yellow glider with a more
  polished-looking placeholder model.

Configuration
-------------

The only way to make settings persistent for now is by adding
arguments to FlightGear commandline or
"Settings / Additional Settings" and changing them to your setup:

    --prop:/nasal/fwhax/printposandfuel/enable=false
    --prop:/nasal/fwhax/toggleterrasync/enable=false
    --prop:/nasal/fwhax/toggleterrasync/alt-m=15250.0
    --prop:/nasal/fwhax/fdmatmosphere/enable=false
    --prop:/nasal/fwhax/jsbsimterrainoverride/enable=false
    --prop:/nasal/fwhax/lockunsafemenu/enable=false
    --prop:/nasal/fwhax/togglefgcom/enable=false
    --prop:/nasal/fwhax/ai_autovolume/enable=false
    --prop:/nasal/fwhax/ai_autovolume/internal=0.1
    --prop:/nasal/fwhax/ai_autovolume/external=1.0
    --prop:/sim/replay/buffer/high-res-time=60.0
    --prop:/sim/replay/buffer/medium-res-time=600.0
    --prop:/sim/replay/buffer/medium-res-sample-dt=0.5
    --prop:/sim/replay/buffer/low-res-time=3600.0
    --prop:/sim/replay/buffer/low-res-sample-dt=5.0

**NOTES:**

1. The options that may affect the realism will display a message in
   your chat, as a reminder in case they are left on by accident.

2. The additional views are disabled by default, because there is no
   way to save their enable/disable flag. You can still access them
   from the replay menu -- which seems to be the only meaningful use
   case.


Extra features
--------------

See the "Extras" folder.

* `ufgl.sh`: micro FGFS launcher for Bourne shell.


Installation
------------

Download and unpack the add-on and point FlightGear to it by using:

- command-line option `--addon=/path/to/fwhax`;

- the "Add-ons / Add-on Module Folders" tab of the launcher.
