# FUNCTIONS


# @brief Dump property tree to "${FG_HOME}/state/properties-dump.xml"
var DumpPropertyTree = func {
 fg_home = getprop("sim/fg-home");
 if(fg_home != nil){
  var path = fg_home ~ "/state/properties-dump.xml";
  if(io.write_properties(path, "/")){
   var msg = "Property tree written to " ~ path;
   gui.popupTip(msg);
   print(msg);
  }
 }
}


# @brief Print position and fuel levels.
# This should give an idea where to restart if FG crashed.
var PrintPosAndFuel = func {
 var pos = geo.aircraft_position();
 print("===== Position, fuel and payload ======");
 print(
  "--lat=" ~ pos.lat() ~
  " --lon=" ~ pos.lon() ~
  " --heading=" ~ getprop("/orientation/heading-deg")
 );
 print(
  "--altitude=" ~ (pos.alt() * M2FT) ~
  " --vc=" ~ getprop("/velocities/airspeed-kt")
 );
 var numtanks = getprop("/consumables/fuel/numtanks") or 0;
 var numtanks = getprop("/consumables/fuel/numtanks") or 0;
 for(var itank = 0; itank < numtanks; itank += 1){
  var name = getprop("/consumables/fuel/tank[" ~ itank ~ "]/name");
  var fuel = getprop("/consumables/fuel/tank[" ~ itank ~ "]/level-lbs");
  print(name ~ " " ~ fuel ~ " lbs");
 }
 var iweight = 0;
 # There is no variable for the number of weights, so we do it this way.
 while(getprop("/payload/weight[" ~ iweight ~ "]/name")){
  var name = getprop("/payload/weight[" ~ iweight ~ "]/name");
  var weight = getprop("/payload/weight[" ~ iweight ~ "]/weight-lb");
  print(name ~ " " ~ weight ~ " lbs");
  iweight += 1;
 }
 print("=======================================");
}

var _PrintPosAndFuel_timer = maketimer(300.17, func{PrintPosAndFuel()});

setlistener("/nasal/fwhax/printposandfuel/enable", func(p) {
 if(p.getValue()){
  _PrintPosAndFuel_timer.start();
 }else{
  _PrintPosAndFuel_timer.stop();
 };
}, 1, 0);


# @brief Disable terrasync above set altitude.
# @param maxalt Maximum altitude for Terrasync enabled.
var ToggleTerrasync = func(maxalt) {
 var enabled = getprop("/sim/terrasync/enabled");
 var command = (geo.aircraft_position().alt() < maxalt);
 if(command != enabled){
  setprop("/sim/terrasync/enabled", command);
  var msg = nil;
  if(command){
   msg = "Enabling Terrasync below " ~ maxalt ~ " m";
  }else{
   msg = "Disabling Terrasync above " ~ maxalt ~ " m";
  }
  gui.popupTip(msg);
  print(msg);
 }
}

var _ToggleTerrasync_timer = maketimer(60.25, func{ToggleTerrasync(getprop("/nasal/fwhax/toggleterrasync/alt-m"))});

setlistener("/nasal/fwhax/toggleterrasync/enable", func(p) {
 if(p.getValue()){
  _ToggleTerrasync_timer.start();
 }else{
  _ToggleTerrasync_timer.stop();
 };
}, 1, 0);


# @brief Use the FDM's built-in atmosphere
# @param enable Enable this hack.
var FDMAtmosphere = func(enable) {
 setprop("/environment/params/control-fdm-atmosphere", 1 - enable);
 if(enable){
  var msg = "HAX! FDM's built-in atmosphere";
  print(msg);
  screen.log.write(msg);
 }
}

setlistener("/nasal/fwhax/fdmatmosphere/enable", func(p) {
 FDMAtmosphere(p.getValue());
}, 1, 0);


# @brief Enable taxi on any terrain (JSBSim only)
# @param enable Enable this hack.
var JSBSimTerrainOverride = func(enable) {
 setprop("/sim/fdm/surface/override-level", enable);
 if(enable){
  setprop("/fdm/jsbsim/ground/solid", 1);
  setprop("/fdm/jsbsim/ground/rolling_friction-factor", 1.0);
  setprop("/fdm/jsbsim/ground/static-friction-factor", 1.0);
  setprop("/fdm/jsbsim/ground/bumpiness", 0.0);
  setprop("/fdm/jsbsim/ground/maximum-force-lbs", 1.45037738e+26);
  var msg = "HAX! JSBSim terrain override";
  print(msg);
  screen.log.write(msg);
 }
}

setlistener("/nasal/fwhax/jsbsimterrainoverride/enable", func(p) {
 JSBSimTerrainOverride(p.getValue());
}, 1, 0);


# @brief Disable unsafe menu items that have no confirmation.
# @param enable Enable this hack.
var LockUnsafeMenu = func(enable) {
 var unlock = 1 - enable;
 gui.menuEnable ("reset", unlock);
 gui.menuEnable ("random-attitude", unlock);
 gui.menuEnable ("aircraft-center", unlock);
 # Crashes FG: https://sourceforge.net/p/flightgear/codetickets/2356/
 gui.menuEnable ("map-canvas", unlock);
}

setlistener("/nasal/fwhax/lockunsafemenu/enable", func(p) {
 LockUnsafeMenu(p.getValue());
}, 1, 0);


var _togglefgcom_listener = nil;

# @brief Sync FGCOM on/off with multiplayer.
# @param enable Enable this hack.
var ToggleFGCOM = func(enable) {
 if(enable){
  setprop("/sim/fgcom/enabled", getprop("/sim/multiplay/online"));
  if(_togglefgcom_listener == nil) {
   _togglefgcom_listener = setlistener("/sim/multiplay/online", func(p) {
    setprop("/sim/fgcom/enabled", p.getValue());
   });
  }
 }else{
  if(_togglefgcom_listener != nil) {
   removelistener(_togglefgcom_listener);
  }
 }
}

setlistener("/nasal/fwhax/togglefgcom/enable", func(p) {
 ToggleFGCOM(p.getValue());
}, 1, 0);


var _ai_autovolume_listener = nil;
settimer(func{ # Workaround for not setting on startup
setlistener("nasal/fwhax/ai_autovolume/enable", func(p) {
 if (p.getValue()) {
  if (_ai_autovolume_listener != nil) {
   return;
  }
 } else {
  if (_ai_autovolume_listener != nil) {
   removelistener(_ai_autovolume_listener);
   _ai_autovolume_listener = nil;
  }
  return;
 }
 _ai_autovolume_listener = setlistener("sim/current-view/internal", func(p) {
  var vol = p.getValue() ? getprop("nasal/fwhax/ai_autovolume/internal") : getprop("nasal/fwhax/ai_autovolume/external");
  setprop("sim/sound/aimodels/volume", vol);
 }, 1, 0);
}, 1, 0);
}, 5.0);
