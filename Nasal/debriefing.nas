# DEBRIEFING DIALOG

var _wow = 1;
var _Hdot = 0.0;
var _takeofftime = 0;


var TouchdownCoords = func() {
 var ap = airportinfo();
 var psi = getprop("/orientation/heading-deg");
 var rwy = 0;
 var score = 90.0;
 var found = 0;
 foreach(var i; keys(ap.runways)){
  var deltapsi_hdg = ap.runways[i].heading - psi;
  while (deltapsi_hdg <= -180.0){
   deltapsi_hdg += 360.0;
  }
  while (deltapsi_hdg > 180.0){
   deltapsi_hdg -= 360.0;
  }
  deltapsi_hdg = abs(deltapsi_hdg);
  # Prefer parallel.
  if(deltapsi_hdg < 90.0){
   found = 1;
   # Look for the minimum side displacement.
   var wp = geo.Coord.new().set_latlon(ap.runways[i].lat, ap.runways[i].lon);
   wp.apply_course_distance(
    ap.runways[i].heading,
    ap.runways[i].threshold
   );
   var (psi_brg, s) = courseAndDistance(wp);
   var deltapsi_brg = ap.runways[i].heading - psi_brg;
   while (deltapsi_brg <= -90.0){
    deltapsi_brg += 180.0;
   }
   while (deltapsi_brg > 90.0){
    deltapsi_brg -= 180.0;
   }
   deltapsi_brg = abs(deltapsi_brg);
   if(deltapsi_brg < score){
    rwy = i;
    score = deltapsi_brg;
   }
  }
 }
 var X = 0.0;
 var Y = 0.0;
 var rating = "";
 if(found){
  var wp = geo.Coord.new().set_latlon(ap.runways[rwy].lat, ap.runways[rwy].lon);
  var psi_rwy = ap.runways[rwy].heading;
  var (psi_brg, D) = courseAndDistance(wp);
  D *= NM2M;
  X = -D * math.sin((psi_rwy - psi_brg) * D2R);
  Y = -D * math.cos((psi_rwy - psi_brg) * D2R);
  rating = "bad";
  if((Y > 150.0) * (Y < 800.0)){
   if(Y < 600.0){
    rating = "excellent";
   }else{
    rating = "good";
   }
  }
 }
 setprop("/nasal/debriefing/touchdown-X-m", X);
 setprop("/nasal/debriefing/touchdown-Y-m", Y);
 setprop("/nasal/debriefing/touchdown-Y-rating", rating);
}


var _loop = func {

 if(getprop("/sim/freeze/replay-state")){
  return;
 }

 var wow = 0;
 for(var ii = 0; ii < 64; ii = ii + 1){
  var contact = getprop("/gear/gear[" ~ ii ~ "]/wow");
  if(contact == nil){
   break;
  }
  if(contact){
   wow = 1;
   break;
  }
 }
 if(wow < _wow){
  _takeofftime = getprop("/sim/time/elapsed-sec");
 }else if(wow > _wow){
  setprop("/nasal/debriefing/touchdown-Hdot-m_s", math.abs(_Hdot) * FT2M);
  TouchdownCoords();
  var seconds = getprop("/sim/time/elapsed-sec") - _takeofftime;
  setprop("/nasal/debriefing/airborne-sec", seconds);
  var hours = int(seconds/3600);
  var minutes = int(math.mod(seconds/60, 60));
  seconds = int(math.mod(seconds, 60));
  setprop("/nasal/debriefing/airborne-string", sprintf("%0u:%02u:%02u", hours, minutes, seconds));
 }
 _wow = wow;

 # SIC Use Hdot from the previous timestep, above. More precise.
 _Hdot = getprop("/velocities/down-relground-fps") or 0.0;

}

var _timer = maketimer(0.11, func{_loop()});


_timer.start();
