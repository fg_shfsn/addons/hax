# RUNWAY VIEW
#
# Realistic cameras on the runway for evaluation of takeoff/landing
# recordings

var _name = "Runway View";
var _name_front = "Runway View Front";

var update = func(front = 0) {
 var ap = airportinfo(getprop("/sim/tower/airport-id"));
 if(ap == nil){
  return nil;
 }
 var psi = getprop("/orientation/heading-deg") + front * 180;
 var rwy = nil;
 var score = 90.0;
 foreach(var i; keys(ap.runways)){
  var deltapsi_hdg = ap.runways[i].heading - psi;
  deltapsi_hdg = abs(geo.normdeg180(deltapsi_hdg));
  # Prefer (anti)parallel.
  if(deltapsi_hdg < 90.0){
   # Look for the minimum side displacement.
   var wp = geo.Coord.new().set_latlon(ap.runways[i].lat, ap.runways[i].lon);
   var (psi_brg, s) = courseAndDistance(wp);
   var deltapsi_brg = ap.runways[i].heading - psi_brg;
   while (deltapsi_brg <= -90.0){
    deltapsi_brg += 180.0;
   }
   while (deltapsi_brg > 90.0){
    deltapsi_brg -= 180.0;
   }
   deltapsi_brg = abs(deltapsi_brg);
   if(deltapsi_brg < score){
    rwy = i;
    score = deltapsi_brg;
   }
  }
 }
 if(rwy != nil){
  var camera = geo.Coord.new().set_latlon(ap.runways[rwy].lat, ap.runways[rwy].lon);
  if(front){
   # Behind the runway.
   camera.apply_course_distance(
    ap.runways[rwy].heading + 180.0,
    50.0
   );
  }else{
   # Touchdown zone.
   camera.apply_course_distance(
    ap.runways[rwy].heading,
    ap.runways[rwy].threshold + 150.0
   );
   # On the side of the runway.
   camera.apply_course_distance(
    ap.runways[rwy].heading - 90.0,
    ap.runways[rwy].width / 2 + 20.0
   );
  }
  var H = geo.elevation(camera.lat(), camera.lon());
  # If no terrain, fall back to airport elevation.
  if (H == nil){
   H = ap.elevation;
  }
  setprop("/nasal/runway_view/latitude-deg", camera.lat());
  setprop("/nasal/runway_view/longitude-deg", camera.lon());
  setprop("/nasal/runway_view/heading-deg", ap.runways[rwy].heading);
  setprop("/nasal/runway_view/altitude-ft", (H + 1.5) * M2FT);
 # Tower view as fallback.
 }else{
  setprop("/nasal/runway_view/latitude-deg", getprop("/sim/tower/latitude-deg"));
  setprop("/nasal/runway_view/latitude-deg", getprop("/sim/tower/longitude-deg"));
  setprop("/nasal/runway_view/altitude-ft", getprop("/sim/tower/altitude-ft"));
 }
}

setlistener("/sim/current-view/name", func(p) {
 var viewname = p.getValue();
 if(viewname == _name){
  update();
 }else if(viewname == _name_front){
  update(1);
 }
});
