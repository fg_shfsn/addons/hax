#!/bin/sh --
#
# ufgl
# Micro FGFS launcher

set -e

EXE="fgfs"
PRECOMMAND="xset m 0 0"
POSTCOMMAND="pgrep fgfs || xset m"
CONFDIR="$HOME/.fgfs/ufgl"
#CONFDIR="$HOME/.config/ufgl"
SAVE="$CONFDIR/save"
ACLIST="$CONFDIR/aclist"
APLIST="$CONFDIR/aplist"
CONFLIST="$CONFDIR/conflist"

prompt ()
{
 QUERY=${1:?}
 LIST=${2:?}
 DEFAULT=$3
 OK=0
 while [ $OK = 0 ]; do
  clear
  i=1
  IFS=$'\n'
  for a in $(grep -v '^#' "$LIST" |sed -e's/#.*$//'); do
   echo "$i. $a"
   i=$((i + 1))
  done
  unset IFS
  echo 'e. Edit this list.'
  read -p"$QUERY [$DEFAULT] > " INPUT
  if [ -z "$INPUT" ]; then
   OUT="$DEFAULT" && OK=1
  else
   if [ "$INPUT" = "e" ]; then
    $VISUAL "$LIST" || $EDITOR "$LIST"
   else
    OUT=$(grep -v '^#' "$LIST" |awk "NR==$INPUT {print}") && [ -z "$OUT" ] || OK=1
   fi
  fi
 done
}

if [ -t 0 ]; then
 INTERACTIVE=1;
else
 INTERACTIVE=0;
fi

if ! [ -d "$CONFDIR" ]; then
 mkdir -p "$CONFDIR"
 echo "Creating configuration dir: $CONFDIR" >&2
 echo >&2
fi
if ! [ -f "$ACLIST" ]; then
 printf 'Tu-144D\n' >>"$ACLIST"
 printf 'Tu-144D --state=take-off\n' >>"$ACLIST"
 printf 'MiG-15bis --enable-auto-coordination\n' >>"$ACLIST"
 printf '# Syntax: <Aircraft> [<FGFS args> #<Comment>]\n' >>"$ACLIST"
 printf '# Each line must start with unique word.\n' >>"$ACLIST"
fi
if ! [ -f "$APLIST" ]; then
 printf 'EDDH\n' >>"$APLIST"
 printf 'EDDS --lat=48.6892 --lon=9.1958 --altitude=1311.10 --heading=343.2\n' >>"$APLIST"
 printf 'KLAS --lat=36.0822 --lon=-115.1509 --altitude=2114.55 --heading=0.0\n' >>"$APLIST"
 printf '# Syntax: <ICAO code> [<FGFS args> #<Comment>]\n' >>"$APLIST"
 printf '# Each line must start with unique word.\n' >>"$APLIST"
fi
if ! [ -f "$CONFLIST" ]; then
 printf '\n' >>"$CONFLIST"
 printf 'DEFAULT --timeofday=noon\n' >>"$CONFLIST"
 printf 'MP1 --callsign=ufgl1 --multiplay=out,10,mpserver01.flightgear.org,5000 --multiplay=in,10,,5001\n' >>"$CONFLIST"
 printf 'MP2 --callsign=ufgl2 --multiplay=out,10,mpserver01.flightgear.org,5000 --multiplay=in,10,,5002 # Multiple sessions per machine\n' >>"$CONFLIST"
 printf 'LH1 --callsign=ufgl1 --multiplay=out,10,localhost,5000 --multiplay=in,10,,5001 # Loopback 1\n' >>"$CONFLIST"
 printf 'LH2 --callsign=ufgl2 --multiplay=out,10,localhost,5001 --multiplay=in,10,,5000 # Loopback 2\n' >>"$CONFLIST"
 printf '# Syntax: <Name> [<FGFS args> #<Comment>]\n' >>"$CONFLIST"
 printf '# Each line must start with unique word.\n' >>"$CONFLIST"
fi
if ! [ -f "$SAVE" ]; then
 printf 'Tu-144D\n' >"$SAVE"
 printf 'EDDS\n' >>"$SAVE"
 printf 'DEFAULT\n' >>"$SAVE"
fi

SAVEAC=$(awk 'NR==1 {print}' "$SAVE")
SAVEAP=$(awk 'NR==2 {print}' "$SAVE")
SAVECONF=$(awk 'NR==3 {print}' "$SAVE")

if [ $INTERACTIVE = 1 ]; then
 prompt Aircraft $ACLIST "$SAVEAC"
 AC=$OUT
 prompt Airport $APLIST "$SAVEAP"
 AP=$OUT
 prompt Configuration $CONFLIST "$SAVECONF"
 CONF=$OUT
 if [ "$AP" != "$SAVEAP" ] || [ "$AC" != "$SAVEAC" ] || [ "$CONF" != "$SAVECONF" ]; then
  cat <<EOF >"$SAVE"
$AC
$AP
$CONF
EOF
 fi
else
 AC=$SAVEAC
 AP=$SAVEAP
 CONF=$SAVECONF
fi
ACARGS=$(echo $AC |sed -e's/#.*$//')
APARGS=$(echo $AP |sed -e's/#.*$//')
CONFARGS=$(echo $CONF |expand |sed -e"s/^$(echo $CONF |cut -d' ' -f 1)  *//" -e's/#.*$//')

$PRECOMMAND
trap "$POSTCOMMAND" 0 2 9 15
if [ -z "$GDB" ]; then
 "$EXE" $ARGS --aircraft=$ACARGS --airport=$APARGS $CONFARGS $@
else
 exec gdb -q -n -batch -ex 'handle SIGTSTP nostop' -ex 'handle SIGCONT nostop' -ex 'handle SIGHUP nostop nopass' -ex 'handle SIGPIPE nostop' -ex r -ex 'thread apply all bt' -ex where --args "$EXE" $ARGS --aircraft=$ACARGS --airport=$APARGS $CONFARGS $@ |tee ~/.fgfs/gdb.log
fi
